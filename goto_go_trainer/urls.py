from django.conf.urls import include, url

from django.contrib import admin
admin.autodiscover()

import core.views

urlpatterns = [
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^$', core.views.index, name='index'),
    url(r'^account/', include('allauth.urls')),
    url(r'^account/profile/', core.views.profile, name='profile'),
    url(r'^account/settings/', core.views.settings, name='settings'),
    url(r'^visits', core.views.visits, name='visits'),
    url(r'^journey/$', core.views.journey, name='journey'),
    url(r'^journey/tutorial/(?P<num>[1-6])', core.views.tutorial, name='tutorial'),
    url(r'^admin/', include(admin.site.urls)),
]
