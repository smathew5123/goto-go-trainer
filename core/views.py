"""
Views for the core app
"""

import logging

from django.shortcuts import render
from .models import Greeting, JourneyNodes
from .queries import get_level, update_go_user

LOGGER = logging.getLogger(__name__)

def index(request):
    """
    Returns the index template
    """
    return render(request, 'index.html')

def settings(request):
    """
    Returns the settings template
    """
    return render(request, 'settings.html')

def profile(request):
    """
    Returns the profile template
    """
    nodes = JourneyNodes.objects.all()
    level = get_level(request.user)
    return render(request, 'profile.html', {'level':level, 'nodes':nodes})

def visits(request):
    """
    Returns the visits template
    Lists all the times the page has been visited
    """
    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, 'visits.html', {'greetings': greetings})

def journey(request):
    """
    Returns the journey template
    """
    if request.method == 'POST' and (not request.user.is_anonymous()):
        if not update_go_user(request.user, request.POST.get("id")):
            LOGGER.error("Update_go_user returned false")

    position = 0
    if not request.user.is_anonymous():
        position = get_level(request.user)
    return render(request, 'journey.html', {'position':('node'+str(position))})

def tutorial(request, num):
    """
    Returns the tutorial page
    """
    return render(request, 'tutorial/tutorial_'+num+'.html')
