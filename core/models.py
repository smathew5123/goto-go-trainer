"""
Models for our website
Temporarily just holds greeting as a test
"""
from django.db import models
from django.contrib.auth.models import User

class Greeting(models.Model):
    """
    Temporary model for keeping track of when users view a page
    """
    when = models.DateTimeField('date created', auto_now_add=True)

class JourneyNodes(models.Model):
    """
    Model for a node on the journey map
    """
    name = models.CharField(max_length=200)
    region = models.CharField(max_length=200)
    link = models.CharField(max_length=200)

    def __str__(self):
        """
        Returns a JourneyNodes object information
        """
        return '%s %s %s' % (self.name, self.region, self.link)

class GoUser(models.Model):
    """
    Model for keeping track of the user's last visited journey node.
    """
    user_id = models.OneToOneField(User, on_delete=models.CASCADE)
    last_completed_node = models.ForeignKey(JourneyNodes)

    def __str__(self):
        """
        Returns a GoUser object information
        """
        return '%s %s' % (self.user_id, self.last_completed_node)

class UserCompletedNodes(models.Model):
    """
    Model for keeping of track of which users completed which nodes.
    """
    user_id = models.ForeignKey(User)
    node_id = models.ForeignKey(JourneyNodes)

    def __str__(self):
        """
        Returns a UserCompletedNodes object information
        """
        return '%s %s' % (self.user_id, self.node_id)
