"""
Adds the lint command to manage.py
Runs a linter on the core app

Invoke by `python manage.py lint`
"""
from os import system
from pylint.lint import Run

from django.core.management.base import BaseCommand

class Command(BaseCommand):
    """
    Required class to work with manage.py
    """
    help = 'Run pylint that accounts for django specific syntax'

    def handle(self, *args, **option):
        # pylint: disable=unused-argument
        Run(["--load-plugins=pylint_django", "core"])
        system("pep8 --exclude='migrations' core")
