"""
File for Acceptance test using Selenium
"""
import time
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

# Create your tests here.
class SeleniumTest(StaticLiveServerTestCase):
    """
    Class for Selenium Testing
    """
    @classmethod
    def setUpClass(cls):
        """
        Constructor
        """
        super(SeleniumTest, cls).setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        """
        Destructor
        """
        cls.selenium.quit()
        super(SeleniumTest, cls).tearDownClass()

    def click_location_id(self, x_val, y_val, location):
        """
        Function to click on a location based on the id
        """
        element = self.selenium.find_element_by_id(location)
        action = ActionChains(self.selenium)
        action.move_to_element_with_offset(element, x_val, y_val)
        action.click()
        action.perform()

#_________________________________
# Tests for the nodes
#_________________________________
    def test_getstarted_button(self):
        """
        Test the if getting started button goes to the correct page
        """
        home_url = self.live_server_url
        self.selenium.get(self.live_server_url)
        self.selenium.find_element_by_class_name("btn-success").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('tutIsland')
        )
        journey_url = self.selenium.current_url
        journey_url = journey_url.replace(home_url, "")
        self.assertEqual(journey_url, '/journey/')

    def test_node1(self):
        """
        Test if node 1 is redirecting properly
        """
        home_url = self.live_server_url
        #go to page
        self.selenium.get('%s%s' % (self.live_server_url, '/journey/'))
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('tutIsland')
        )
        #click on tutIsland
        self.selenium.find_element_by_id("tutIsland").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('node1')
        )
        #click on node
        self.selenium.find_element_by_id("node1").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_class_name('buts')
        )
        #check to see if its correct
        journey_url = self.selenium.current_url
        journey_url = journey_url.replace(home_url, "")
        self.assertEqual(journey_url, '/journey/tutorial/1')

    def test_node2(self):
        """
        Test if node 2 is redirecting properly
        """
       #go to page
        self.selenium.get('%s%s' % (self.live_server_url, '/journey/'))
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('tutIsland')
        )

        #click on tutIsland
        self.selenium.find_element_by_id("tutIsland").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('node2')
        )

        #click 2nd node
        self.selenium.execute_script(
            """
            $("#node2").click(function(){
                window.location = '/journey/tutorial/2';
            });
            """
        )
        self.selenium.find_element_by_id("node2").click()

        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_class_name('buts')
        )
        #validate we are on the right page
        element = self.selenium.find_element_by_tag_name('h2').text
        self.assertEqual("Liberties and Capturing Enemy Stones", element)

    def test_node3(self):
        """
        Test if node 3 is redirecting properly
        """
        #go to page
        self.selenium.get('%s%s' % (self.live_server_url, '/journey/'))
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('tutIsland')
        )
        #click on tutIsland
        self.selenium.find_element_by_id("tutIsland").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('node3')
        )
        #click 3nd node
        self.selenium.execute_script(
            """
            $("#node3").click(function(){
                window.location = '/journey/tutorial/3';
            });
            """
        )
        self.selenium.find_element_by_id("node3").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_class_name('buts')
        )
        #validate we are on the right page
        element = self.selenium.find_element_by_tag_name('h2').text
        self.assertEqual("Saving your Own Stones", element)


    def test_node4(self):
        """
        Test if node 4 is redirecting properly
        """
        #go to page
        self.selenium.get('%s%s' % (self.live_server_url, '/journey/'))
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('tutIsland')
        )
        #click on tutIsland
        self.selenium.find_element_by_id("tutIsland").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('node4')
        )
        #click 4th node
        self.selenium.execute_script(
            """
            $("#node4").click(function(){
                window.location = '/journey/tutorial/4';
            });
            """
        )
        self.selenium.find_element_by_id("node4").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('text')
        )
        #validate we are on the right page
        element = self.selenium.find_element_by_tag_name('h2').text
        self.assertEqual("Illegal Moves", element)

    def test_node5(self):
        """
        Test if node 5 is redirecting properly
        """
        #go to page
        self.selenium.get('%s%s' % (self.live_server_url, '/journey/'))
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('tutIsland')
        )
        #click on tutIsland
        self.selenium.find_element_by_id("tutIsland").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('node5')
        )
        #click 5nd node
        self.selenium.execute_script(
            """
            $("#node5").click(function(){
                 window.location = '/journey/tutorial/5';
            });
            """
        )
        self.selenium.find_element_by_id("node5").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('text')
        )
        #validate we are on the right page
        element = self.selenium.find_element_by_tag_name('h2').text
        self.assertEqual("Mutual Atari", element)

    def test_node6(self):
        """
        Test if node 6 is redirecting properly
        """
        #go to page
        self.selenium.get('%s%s' % (self.live_server_url, '/journey/'))
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('tutIsland')
        )
        #click on tutIsland
        self.selenium.find_element_by_id("tutIsland").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('node6')
        )
        #click 2nd node
        self.selenium.execute_script(
            """
            $("#node6").click(function(){
                window.location = '/journey/tutorial/6';
            });
            """
        )
        self.selenium.find_element_by_id("node6").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('text')
        )
        #validate we are on the right page
        element = self.selenium.find_element_by_tag_name('h2').text
        self.assertEqual("Ko", element)


#_________________________________
#Tests to see if the tutorial node colors are correct
#_________________________________

    def test_node_color(self):
        """
        Tests the colors
        """
        self.selenium.get('%s%s' % (self.live_server_url, '/journey/'))
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('tutIsland')
        )
        #click on tutIsland
        self.selenium.find_element_by_id("tutIsland").click()
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('node1')
        )

        element = self.selenium.find_element_by_id('node1')
        self.assertEqual(element.get_attribute('fill'), "url(#yellow)")
        element = self.selenium.find_element_by_id('node2')
        self.assertEqual(element.get_attribute('fill'), "url(#grad1)")
        element = self.selenium.find_element_by_id('node3')
        self.assertEqual(element.get_attribute('fill'), "url(#grad1)")


#_________________________________
#Test to see if tutorial is working
#_________________________________
    def test_tutorial(self):
        """
        Test to see if board is working
        """
        self.selenium.get('%s%s' % (self.live_server_url, '/journey/tutorial/1'))
        WebDriverWait(self.selenium, 3).until(
            lambda driver: driver.find_element_by_id('tsumego_id')
        )
        self.click_location_id(300, 250, 'tsumego_id')
        time.sleep(2)
        self.selenium.find_element_by_id("tut_1_button").click()
        time.sleep(2)
        text = self.selenium.find_element_by_id("tut_1_text").text
        self.assertTrue("Don't question them! Just be happy you're not" in text)

        #go through second part of the tutorial
        self.click_location_id(300, 250, 'tsumego_id')
        time.sleep(2)
        self.selenium.find_element_by_id("tut_1_button").click()
        time.sleep(2)
        text = self.selenium.find_element_by_id("tut_1_text").text
        self.assertTrue("We discovered four important locations" in text)

        #go through third part of the tutorial
        self.click_location_id(250, 200, 'tsumego_id')
        time.sleep(2)
        board = self.selenium.find_element_by_id("tsumego_id")
        action = ActionChains(self.selenium)
        action.move_to_element_with_offset(board, 350, 300)
        action.click()
        action.perform()
        time.sleep(2)
        self.selenium.find_element_by_id("tut_1_button").click()
        time.sleep(2)
        text = self.selenium.find_element_by_id("tut_1_text").text
        self.assertTrue("Alas, life is not so simple." in text)

        #go through fourth part of the tutorial
        time.sleep(2)
        self.selenium.find_element_by_id("tut_1_button").click()
        text = self.selenium.find_element_by_id("tut_1_text").text
        self.assertTrue("We need to figure out who won!" in text)

        #go through fifth part of the tutorial
        self.click_location_id(200, 375, 'tsumego_id')
        time.sleep(2)
        self.selenium.find_element_by_id("tut_1_button").click()
        time.sleep(2)
        text = self.selenium.find_element_by_id("tut_1_text").text
        self.assertTrue("Well, let's send one down there anyway." in text)

        #go through sixth part of the tutorial
        self.click_location_id(375, 175, 'tsumego_id')
        time.sleep(2)
        text = self.selenium.find_element_by_tag_name("body").text
        self.assertTrue("Correct!" in text)
