"""
Queries in database for the core app
"""
import logging

from .models import JourneyNodes, GoUser

def get_level(user):
    """
    Returns the highest level user completed
    """
    try:
        obj = GoUser.objects.get(user_id=user)
        return obj.last_completed_node.pk
    except GoUser.DoesNotExist:
        return 0

def update_go_user(user, id_num):
    """
    Create Go User object for user if it does not exist with the latest completed node
    Go User object's last_compeleted_node is updated
    if id_num is greater than the current JourneyNodes object's pk
    """
    try:
        obj = GoUser.objects.get(user_id=user)
        if int(id_num) > obj.last_completed_node.pk:
            try:
                obj.last_completed_node = JourneyNodes.objects.get(pk=id_num)
            except JourneyNodes.DoesNotExist:
                logging.error('Cannot find JourneyNodes object with pk %s', id_num)
                return False
    except GoUser.DoesNotExist:
        try:
            obj = GoUser(user_id=user,
                         last_completed_node=JourneyNodes.objects.get(pk=id_num))
        except JourneyNodes.DoesNotExist:
            logging.error('Cannot find JourneyNodes object with pk %s', id_num)
            return False
    obj.save()
    return True
